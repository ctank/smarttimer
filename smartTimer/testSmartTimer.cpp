/*
 * testSmartTimer.cpp
 *
 * Created: 13/6/2015 8:56:54 PM
 *  Author: Colin Tan
 */ 

#include <Arduino.h>
#include "smartTimer.h"

void blink(char pin, char flag)
{
	if(flag)
		digitalWrite(pin, HIGH);
	else
		digitalWrite(pin, LOW);	
}

void call1()
{
	Serial.println("1");
}

void call2()
{
	Serial.println("2");
}

void call3()
{
	static char flag=0;
	blink(13, flag);
	flag=!flag;
}

void setup()
{
	pinMode(11, OUTPUT);
	pinMode(12, OUTPUT);
	pinMode(13, OUTPUT);

	Serial.begin(9600);
	CSmartTimer *timer=new CSmartTimer(STIMER3);
	
	#ifdef RTC
	timer->RTCSetUTCOffset(8, 0);
	timer->RTCSetLocalTime(14, 6, 2015, 15, 49, 0);
	#endif
	timer->attachCallback(call1, 250); // 250 ms
	timer->attachCallback(call2, 500);
	timer->attachCallback(call3, 1000);
	timer->startTimer();
}

int main()
{
	init();
	setup();
	// Loop
	while(1);
}